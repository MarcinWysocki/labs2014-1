﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {

        }

        public IList<IBase> GetCollection()
        {
            return new List<IBase>(){
                new Impl1(),
                new Impl2(),
                new Impl1(),
                new Impl1(),
                new Impl2(),
                new Impl1(),
            };
        }

        public IList<IBase> DoOperation(IList<IBase> list)
        {
            foreach (IBase item in list)
            {
                item.BaseMethod();
            }

            return list;
        }
    }
}
