﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Impl3: IOther, ISub1
    {
        public Impl3()
        {

        }


        public string GetString()
        {
            return "a";
        }

        public void BaseMethod()
        {
            
        }

        public char GetChar()
        {
            return 'a';
        }

        public void Sub1Method()
        {
            
        }

        public int GetInt()
        {
            return 0;
        }

        int IBase.GetInt()
        {
            return 1;
        }

        int IOther.GetInt()
        {
            return 2;
        }

    }
}
