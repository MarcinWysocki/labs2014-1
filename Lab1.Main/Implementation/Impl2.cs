﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Impl2 : ISub2
    {
        public Impl2()
        {

        }

        public void Sub2Method()
        {
            
        }

        public void BaseMethod()
        {
            
        }


        public int GetInt()
        {
            return 0;
        }

        public char GetChar()
        {
            return 'b';
        }
    }
}
