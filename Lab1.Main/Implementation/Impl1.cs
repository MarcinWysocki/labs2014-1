﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Impl1 : ISub1
    {
        public Impl1()
        {

        }

        public void Sub1Method()
        {
           
        }

        public void BaseMethod()
        {
            
        }


        public int GetInt()
        {
            return 2;
        }

        public char GetChar()
        {
            return 'c';
        }
    }
}
